<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxController
 *
 * @author emman
 */

class ajaxController{
    
    private $_model;
    
    public function __construct() {
        
        // pour ajax, on charge toutes les ressources dans le constructeur  
        require_once '../core/Model.php';
        require_once '../models/ajaxModel.php';
        $this->model = new ajaxModel();
    }
    
    public function checkDate($date) {
        
        if($this->model->checkdate($date)){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function getTechData(){
        
         
         $tech_data = $this->model->getTechData();
         return $tech_data;
    }
    

    public function insertInterventions($interventions, $dayInter){
        
        
      if($this->model->insertInterventions($interventions, $dayInter)){
          
          
          
            return true;
        }
    }
    
    
     public function updateInterventions($interventions, $dayInter){
        
     
        
        if($this->model->updateInterventions($interventions, $dayInter)){
            return true;
        }
        
    }
    
   
    public function getDayData($day) {
        
        $dayDatas = $this->model->getDayData($day);
        
        return $dayDatas; 
    }
    
    
    public function getPlageInterventions($day_start, $day_end){
        
        $interventions = $this->model->get_plage_interventions($day_start, $day_end);
        return $interventions;
    }
    
    
    
    public function get_cumul_interventions($dayStart, $dayEnd){
        
        $cumuls['interventions'] = $this->model->get_cumul_interventions($dayStart, $dayEnd);
        $cumuls['techniciens'] = $this->model->get_techniciens();
        return $cumuls;
    }
    
    
    public function get_prestas(){
        
        $prestas = $this->model->get_prestas();
        
        return $prestas;
    }
    
      public function get_techniciens(){
        
        $techs = $this->model->get_techniciens();
        
        return $techs;
    }
    
    
    public function add_presta($presta_name, $presta_msg){
        
        
        $test = $this->model->add_presta($presta_name, $presta_msg); 
        
        return $test;
    }
    
     public function add_tech($tech_data){
        
        
        $insert_tech = $this->model->add_tech($tech_data); 
        
        return $insert_tech;
    }
    
    public function get_vols( ){
        
        $vols = $this->model->get_vols();
        
        return $vols;
    }
    
    
    public function update_vol($vol){
        
        $update = $this->model->update_vol($vol);
        
        return $update;
    }
    
   
}
