<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of boardController
 *
 * @author emman
 */
class boardController extends Controller {
    
    public function __construct( ){
        $this->model('boardModel');
    }
    
    
    public function index(){
        $this->view('structure/header');
        $this->view('panel/adminpanel');
        $this->view('structure/footer_board');
    }
    
    public function presta(){
        $this->view('structure/header');
        $this->view('panel/prestapanel');
        $this->view('structure/footer');
    }
    
     public function vol(){
        $this->view('structure/header');
        $this->view('panel/vols');
        $this->view('structure/footer_vols');
    }
    
    public function intervention(){
        
           if (isset($_SESSION) && isset($_SESSION['role'])) { 
          
          $this->view('admin/calendarMonthView');
          
      }else{
          $this->view('login/loginView');
      }
    }
    
}
