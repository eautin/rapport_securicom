<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class adminController extends Controller {
    

    public function __construct(){
        $this->model('adminModel');
    }
    
    
    public function index() {
        
    }
    
    
    public function loadSelect(){
        
        $this->view('admin/adminView');
    }
    
    public function displayTechCalendar(){
        
       $id_tech = $_POST['technicien'];
       $adminModel = $this->model('adminModel');
       $tech_data = $adminModel->getTechData($id_tech);
       
       // chargement vue calendrier mois courant. 
       
       
       //$this->view('admin/interventionView', $tech_data);
       
       $this->view('admin/calendarMonthView', $tech_data);
       
    }
    
    
    public function saveIntervention() {
        
         $adminModel = $this->model('adminModel');
         $intervention = $_POST;
         
         if($adminModel->saveIntervention($intervention)){
             
           
             $this->view('admin/interventionValid');
             
         }
         
         
    }
    
    
    public function checkIntervention(){
        
        return true;
    }
    
    
    public function addTech() {
        
    }
    
}