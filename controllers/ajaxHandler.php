<?php
// fichier qui réceptionne les requêtes ajax POST et transfert au ajaxController. 

require_once 'ajaxController.php';
 $ajaxController = new ajaxController();

if(isset($_POST['testAjax'])){
    
 $date = $_POST['date'];

 $response = array (
     'date' => $date,
     'statut' => "",
     'tech_data' => "",
     'tech_inter'=> ""
 );
 
 if($ajaxController->checkDate($date)) {
     
     //intervention a cette date.
    $response['statut'] = true;
    $response['tech_data'] = $ajaxController->getTechData();
    $response['tech_inter'] = $ajaxController->getDayData($date);
    
 }else{
     // pas intervention a cette date. 
     
     $tech_data = $ajaxController->getTechData();
     $response['statut'] = false;
     $response['tech_data'] = $tech_data;
     $response['tech_inter'] = null;
 }
   
    $response = json_encode($response);
    
    echo $response;
}


if(isset($_POST['savenewintervention'])){
    
   
   $interventions = $_POST['intervention'];
   $dayInter = $_POST['date'];
   $ajaxController->insertInterventions($interventions, $dayInter);
  
 
}

if(isset($_POST['saveeditedinformation'])){
    
   $interventions = $_POST['intervention'];
   $dayInter = $_POST['date'];
   $ajaxController->updateInterventions($interventions, $dayInter);
}


// pour les cumuls : 


//si un jour cliqué
if(isset($_POST['getDayInter'])){
    
    $dayStart = $_POST['dayStart'];
    $ajaxController->getDayData($date);
}


if(isset($_POST['getInterventions'])){
    
    $dayStart = $_POST['dayStart'];
    $dayEnd = $_POST['dayEnd'];
    
    //$data['plage_interventions'] = $ajaxController->getPlageInterventions($dayStart, $dayEnd);
    
    $cumuls = $ajaxController->get_cumul_interventions($dayStart, $dayEnd);
    
    echo json_encode($cumuls);

}

if(isset($_POST['addpresta']) && isset($_POST['prestaname'])){
    
    $presta_name = $_POST['prestaname'];
    
    if(isset($_POST['prestamsg'])){
        $presta_msg = $_POST['prestamsg'];
    }else{
        $presta_msg = "non renseigné";
    }
    
    if($ajaxController->add_presta($presta_name, $presta_msg)){
        
        $resp = $ajaxController->get_prestas();

        echo json_encode($resp);
    }
    
}


if(isset($_POST['addtech']) && isset($_POST['techname']) && isset($_POST['techprename'])){
    
    $tech_data = array();
    
    $tech_data['name'] = $_POST['techname'];
    $tech_data['prename'] =  $_POST['techprename'];
    $tech_data['date_creation'] = date('Y-m-d');
    $tech_data['statut'] = "interne";

 
    if($ajaxController->add_tech($tech_data)){
        
        $resp = $ajaxController->get_techniciens();

        echo json_encode($resp);
    }
    
}


if(isset($_POST['getallpresta'])){
    
    $prestas = $ajaxController->get_prestas( );
    $json_prestas = json_encode($prestas);
    $prestataires = array();
    $prestataires['prestataires'] = $ajaxController->get_prestas( );

    echo json_encode($prestataires);
    
}


if(isset($_POST['getalltech'])){
    
 
    $techniciens = array();
    $techniciens['techniciens'] = $ajaxController->get_techniciens( );

    echo json_encode($techniciens);
    
}


if(isset($_POST['getallvols'])){
    
    $vols = array();
    $vols['vols'] = $ajaxController->get_vols( );
    echo json_encode($vols);
}


if(isset($_POST['updatevol']) && isset($_POST['data'])) {
    
    //var_dump($_POST['data']['idVol']);
    $vol_update = $_POST['data'];
    $resp = array( );
    $resp['resp_update'] = $ajaxController->update_vol($vol_update);
    $resp['vols'] = $ajaxController->get_vols( );
    
    echo json_encode($resp);
}