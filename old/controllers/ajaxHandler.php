<?php
// fichier qui réceptionne les requêtes ajax POST et transfert au ajaxController. 

require_once 'ajaxController.php';
 $ajaxController = new ajaxController();

if(isset($_POST['testAjax'])){
    
 $date = $_POST['date'];

 $response = array (
     'date' => $date,
     'statut' => "",
     'tech_data' => "",
     'tech_inter'=> ""
 );
 
 if($ajaxController->checkDate($date)) {
     
     //intervention a cette date.
    $response['statut'] = true;
    $response['tech_data'] = $ajaxController->getTechData();
    $response['tech_inter'] = $ajaxController->getDayData($date);
    
 }else{
     // pas intervention a cette date. 
     
     $tech_data = $ajaxController->getTechData();
     $response['statut'] = false;
     $response['tech_data'] = $tech_data;
     $response['tech_inter'] = null;
 }
   
    $response = json_encode($response);
    
    echo $response;
}


if(isset($_POST['savenewintervention'])){
    
   //var_dump($_POST[intervention]);
   $interventions = $_POST['intervention'];
   $dayInter = $_POST['date'];
   $ajaxController->insertInterventions($interventions, $dayInter);
  
}

if(isset($_POST['saveeditedinformation'])){
    
   $interventions = $_POST['intervention'];
   $dayInter = $_POST['date'];
   $ajaxController->updateInterventions($interventions, $dayInter);
}