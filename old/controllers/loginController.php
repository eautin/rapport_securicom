<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author emman
 */
class LoginController extends Controller {
    //put your code here
    
    public function index( ){
        
      
        $this->view('login/loginView');
        
    }
      
     public function logUser() {
         
         
         //admin : tsip TSIP2018*

        if (isset($_POST['nameUser'])&& isset($_POST['pwdUser'])) {
            
            $pass = md5($_POST['pwdUser']);
           
            $user = $_POST['nameUser']; 
            $loginModel = $this->model("LoginModel");

            if ($loginModel->validateLogin($user, $pass)) {
                
                $role = $loginModel->getRole($user);
                $this->startSession($user, $role);

                if( $role === "admin" ){
                   
                    $adminModel = $this->model('adminModel');
                    
                   // $techData = $adminModel->getTechList();
                   // $this->view('admin/adminView', $techData);
                    $this->view('admin/calendarMonthView');
                    
                }elseif($role === "tech") {
                    
                    // charger la vue tech 
                    $this->view('tech/techView');
                }
                
              
            } else {
                
                $errorMessage = '<div class="alert alert-danger"><strong>Login ou mot de passe incorrect</strong></div>';
                $this->view('login/loginView', $errorMessage);
            }
        } else {
            $this->view('login/loginView');
        }
    }
    
    public function startSession($user, $role){
        
        $_SESSION['name'] = $user;
        $_SESSION['role'] = $role;
        
    }
    
    public function keepSession(){
        
         session_start();
    }
    
    public function logout() {
        
         // Démarrage ou restauration de la session
         //session_start();
         // Réinitialisation du tableau de session
         // On le vide intégralement
         $_SESSION = array();
        // Destruction de la session
        session_destroy();
        // Destruction du tableau de session
        unset($_SESSION);
        // charge vu de connexion
        $this->view('login/loginView');
    }
}
