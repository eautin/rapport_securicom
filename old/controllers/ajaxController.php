<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxController
 *
 * @author emman
 */
class ajaxController{
    
    
    public function __construct() {
        
        // pour ajax, on charge toutes les ressources dans le constructeur
        
        require_once '../core/Model.php';
        require_once '../models/ajaxModel.php';
    }
    
    public function checkDate($date) {
        

        $ajaxModel = new ajaxModel();
       
        if($ajaxModel->checkdate($date)){
            return true;
        }else{
            return false;
        }
    }
    
    
    public function getTechData( ){
        
         $ajaxModel = new ajaxModel();
         $tech_data = $ajaxModel->getTechData();
         return $tech_data;
    }
    
    
    public function updateIntervention(){
        
    }
    
    
    public function insertInterventions($interventions, $dayInter){
        
        $ajaxModel = new ajaxModel();
        
        if($ajaxModel->insertInterventions($interventions, $dayInter)){
            return true;
        }
        
    }
    
    
     public function updateInterventions($interventions, $dayInter){
        
        $ajaxModel = new ajaxModel();
        
        if($ajaxModel->updateInterventions($interventions, $dayInter)){
            return true;
        }
        
    }
    
    
    
    public function getDayData($day) {
        
        $ajaxModel = new ajaxModel();
        
        $dayDatas = $ajaxModel->getDayData($day);
        
        return $dayDatas; 
    }
    
    
}
