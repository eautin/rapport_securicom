
<script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/js/bootstrap.min.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/js/moment.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/js/fullcalendar.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/locale/fr.js"></script>
<script>

    $(function () {

  var currDate = {day: ""};
    

var prod = true;

  if(prod === true){
                        var urlAjax = "/controllers/ajaxHandler.php";
                    }else{
                        var urlAjax = "/intervention_securicom/controllers/ajaxHandler.php";
                    }
                    


function buildTechRow(name, prename, techId, interventions) {
    
    var typeInter = ["sav_intrusion", "sav_incendie", "sav_controle_acces", "sav_video", "sav_agression","sav_suite", "maintenance_intrusion", "maintenance_incendie", "maintenance_controle_acces", "maintenance_video", "maintenance_agression","maintenance_suite", "installation_intrusion", "installation_incendie", "installation_controle_acces", "installation_video", "installation_agression","installation_suite"];
    var row = "<tr>";
    var name = name;
    var prename = prename; 
    
  
    
    row += "<td>"+name+" "+prename+"</td>"
    
   
    var store = [
        '<td><input type="number" data-id="'+techId+'" name="sav_intrusion" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="sav_incendie" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="sav_controle_acces" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="sav_video" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="sav_agression" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="sav_suite" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="maintenance_intrusion" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="maintenance_incendie" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="maintenance_controle_acces" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="maintenance_video" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="maintenance_agression" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="maintenance_suite" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="installation_intrusion" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="installation_incendie" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="installation_controle_acces" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="installation_video" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="installation_agression" value="0" class="form-control" min="0"></td>',
        '<td><input type="number" data-id="'+techId+'" name="installation_suite" value="0" class="form-control" min="0"></td>'
        
    ];
    $.each(typeInter,function(cle,val){

        interventions.forEach(function(arrayItem){
            if(arrayItem.inter == val){
               store[cle] = '<td><input type="number" data-id="'+techId+'" name="'+arrayItem.inter+'" value="'+arrayItem.nb_intervention+'" class="form-control" min="0"></td>';
            }
   
        });

    });
    
    
    row += store[0]+store[1]+store[2]+store[3]+store[4]+store[5]+store[6]+store[7]+store[8]+store[9]+store[10]+store[11]+store[12]+store[13]+store[14]+store[15]+store[16]+store[17]+store[18];
    row += "</tr>";
    
    return row;
    
}
        if ($('#calendar').length != 0) {

            $('#calendar').fullCalendar({
                // put your options and callbacks here
                locale: 'fr',
                dayClick: function (date) {
                
                    var date = date.format( );
                    currDate.day = date;
                    $('#formInjector').empty();
                    $('#formInjectorEdit').empty();
                    $('#modal-resp-edit-success').hide();
                    $('#modal-resp-edit-success').empty();
                    $('#modal-resp-saisie-success').hide();
                    $('#modal-resp-saisie-success').empty();
                    
                    $('#modal-resp-saisie-success').empty();
                    $('#saveEditDate').show();
                    $('#saveNewDate').show();
                    
                    
                    
                    $('#dynamicDayEdit').attr('data-day','');
                    $('#dynamicDay').attr('data-day','');
                    
                    $('#dynamicDayEdit').attr('data-day',date);
                    $('#dynamicDay').attr('data-day', date);
                    
                     
                   // console.log("date envoyée : "+ date);

                    $.ajax({
                        type: "POST",
                        data: {
                            testAjax: "verification si la date a deja ete saisie",
                            date: date
                        },
                        url: urlAjax,
                        success: function (data) { 
                            try{   
                            var response = JSON.parse(data)
                            } catch (e) {
                                        console.error(e)
                                        console.error('JSON recived :', data)
                            }

                            var dateFR = moment(response.date).format('dddd Do MMMM YYYY');
                            
                            console.log(response);
                            
                            // date deja saisie
                            if (response.statut === true) {

                               // alert(response.date);
                                
                                $('#dynamicDayEdit').html(dateFR);
                                //$('#dynamicDayEdit').attr('data-day', response.date);
   
                                var formHTMLEdit = "";
                                $.each(response.tech_data, function (key, technicien) {
     
                                     //pour chaque tech on construit la ligne
                                     // on rÃ©cupÃ©re les interventions du tech:
                                   
                                     var name = technicien.nom;
                                     var prename = technicien.prenom;
                                     var interventionTech = [];
                                     
                                     $.each(response.tech_inter, function(key, intervention){
                                         if(intervention.id_technicien == technicien.id){
                                             var inter = intervention.type_intervention;
                                             var nb_intervention = intervention.nb_intervention;
                                             interventionTech.push({inter, nb_intervention});
                                         }
                                     }); 
                                    
                                     var techId = technicien.id;
                                     var row = buildTechRow(name, prename, techId, interventionTech);
                                     $('#formInjectorEdit').append(row);
                                });

                                //$('#formInjectorEdit').append(formHTMLEdit);
                                $('#modalEdit').modal( );


                            } else {

                                // Mode new day

                                $('#dynamicDay').html(dateFR);
                                
                                //alert("reponse date : "+response.date);
                               // $('#dynamicDay').attr('data-day', response.date);
                                
                                // on réaffiche les boutons etc et on cache la div reponse :
                                $('#modal-resp-saisie-success').empty();
                                $('#formInjector').empty();
                              
                                $('#saveNewDate').show();
                        
                                //console.log('key'+key+' value '+value.id);

                                var formHTML = '';

                                $.each(response.tech_data, function (key, value) {

                                    formHTML += '<tr>';
                                    formHTML += '<td>' + value.nom + " " + value.prenom + '</td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_intrusion" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_incendie" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_controle_acces" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_video" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_agression" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="sav_suite" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_intrusion" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_incendie" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_controle_acces" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_video" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_agression" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="maintenance_suite" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_intrusion" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_incendie" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_controle_acces" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_video" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_agression" class="form-control" value="0" min="0"></td>';
                                    formHTML += '<td><input type="number" data-id="' + value.id + '" name="installation_suite" class="form-control" value="0" min="0"></td>';
                                    formHTML += '</tr>';
                                });

                                $('#formInjector').append(formHTML);
                                $('#modalSaisie').modal( );

                            }

                        }

                    });

                    //if rempli on affiche la popup table prÃ©remplie avec les anciennes valeurs. 

                    //if pas rempli on affiche la popup table

                }

            })


            $('#saveEditDate').click(function () {

                //var currDay = $('#dynamicDayEdit').data('day');
                var currDay = currDate.day;
                
               // alert("save edit currDay : "+currDay);
                
                $inputs = $('[type=number]');
                
                 var interventions = $inputs.map(function () {

                    var val = $(this).val();
                    

                        return {

                            techid: $(this).data('id'),
                            name: $(this).attr('name'),
                            value: $(this).val()
                        }
                    
                }).get( );
                
                $.ajax({
                    type: "POST",
                    data: {
                        saveeditedinformation: "update des interventions en bdd",
                        intervention: interventions,
                        date: currDay
                    },
                    url: urlAjax,
                    success: function (data) {

                       // console.log(data);
                        
                        //$('#formInjector').empty();
                        //$('#formInjectorEdit').empty();
                        
                        $success = '<div class="alert alert-success"><strong>Mise à jour des interventions sauvegardés.</strong></div>';
                       // console.log(data);
                        $('#modal-resp-edit-success').append($success);
                        $('#modal-resp-edit-success').show();
                    
                        //$('#formInjector').empty();
                        
                        $('#saveEditDate').hide();
                        $('#dynamicDayEdit').attr('data-day','');
                        $('#dynamicDay').attr('data-day','');
                        

                    }
                    

                });
                

            });

            $('#saveNewDate').click(function ( ) {

                //get curr date :
                //var currDay = $('#dynamicDay').data('day');
                var currDay = currDate.day;
               // alert("save new date currDay : "+currDay);
                
                //rÃ©cupÃ©rer tous les data-id
                $inputs = $('[type=number]');

                var interventions = $inputs.map(function () { 

                    var val = $(this).val();
                    if (val != 0) {

                        return {

                            techid: $(this).data('id'),
                            name: $(this).attr('name'),
                            value: $(this).val()
                        }
                    }
                }).get();


                $.ajax({
                    type: "POST",
                    data: {
                        savenewintervention: "envoi des interventions en bdd",
                        intervention: interventions,
                        date: currDay
                    },
                    url: urlAjax,
                    success: function (data) {
                        
                        $success = '<div class="alert alert-success"><strong>Succès de l\'enregistrement !</strong></div>';
                       // console.log(data);
                        $('#modal-resp-saisie-success').append($success);
                        $('#modal-resp-saisie-success').show();
                        //$('#formInjector').empty();
                        //$('#formInjectorEdit').empty();
                        $('#saveNewDate').hide();
                        $('#dynamicDayEdit').attr('data-day','');
                        $('#dynamicDay').attr('data-day','');
                        

                    }

                });




            });


        }

    });
    
</script>
  </div><!--body content-->
<div class="modal fade" tabindex="-1" role="dialog" id="modalSaisie">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="popinTitle" data-date="" class="modal-title">Saisie des interventions <span  data-day="" id="dynamicDay"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <th></th>
                    <th>SAV intrusion</th>
                    <th>SAV Incendie</th>
                    <th>SAV Contrôle d'accès</th>
                    <th>SAV vidéo</th>
                    <th>SAV agression</th>
                    <th>SAV suite à prévoir</th>
                    <th>Maintenance Intrusion</th>
                    <th>Maintenance Incendie</th>
                    <th>Maintenance Contrôle accès</th>
                    <th>Maintenance vidéo</th>
                    <th>Maintenance agression</th>
                    <th>Maintenance suite à prévoir</th>
                    <th>Installation Intrusion</th>
                    <th>Installation incendie</th>
                    <th>Installation contrôle d'accès</th>
                    <th>Installation vidéo</th>
                    <th>Installation agression</th>
                    <th>Installation suite à prévoir</th>
                    </thead>
                    <tbody id="formInjector">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="saveNewDate">Sauvegarder</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
                
                <div id="modal-resp-saisie-success">
                    
                </div>
                <div id="modal-resp-saisie-success">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalEdit">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="popinTitle" data-date="" class="modal-title">Saisie des interventions <span  data-day="" id="dynamicDayEdit"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <th></th>
                    <th>SAV intrusion</th>
                    <th>SAV Incendie</th>
                    <th>SAV Contrôle d'accès</th>
                    <th>SAV vidéo</th>
                    <th>SAV agression</th>
                    <th>SAV suite à prévoir</th>
                    <th>Maintenance Intrusion</th>
                    <th>Maintenance Incendie</th>
                    <th>Maintenance Contrôle accès</th>
                    <th>Maintenance vidéo</th>
                    <th>Maintenance agression</th>
                    <th>Maintenance suite à prévoir</th>
                    <th>Installation Intrusion</th>
                    <th>Installation incendie</th>
                    <th>Installation contrôle d'accès</th>
                    <th>Installation vidéo</th>
                    <th>Installation agression</th>
                     <th>Installation suite à prévoir</th>
                    </thead>
                    <tbody id="formInjectorEdit">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="saveEditDate">Mettre à jour</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
                    <div id="modal-resp-edit-success">
                        
                    </div>
                    <div id="modal-resp-edit-error">
                        
                    </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <p class="mention">2018 - rapport.securicom-telesurveillance.fr</p>
    <p class="mention">support : eautin@groupsecuricom.fr</p>
</footer>
</body>

</html>
