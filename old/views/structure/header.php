<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Rapport d'intervention TSIP</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="<?php echo CONTROLLER_PATH; ?>views/assets/css/style.css">
  <link rel="stylesheet" href="<?php echo CONTROLLER_PATH; ?>views/assets/css/bootstrap.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
</head>
<body>
    <header>
        <div class="container ">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <img src="<?php echo CONTROLLER_PATH;?>views/assets/img/securicom.png" />
                    <h1>- Rapport journalier d'activité et d'intervention -</h1>
                </div>
                <div class="col-lg-3">
                     <?php if (isset($_SESSION) && isset($_SESSION['role'])) { ?>
                            <a class="logout" href="<?= CONTROLLER_PATH;?>loginController/logout"><i class="fas fa-sign-out-alt"></i>Déconnexion</a>
                     <?php } ?> 
                </div>
            </div>
        </div>
    </header>
    <div class="body-content">
        
  