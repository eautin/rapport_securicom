
<?php require_once ROOT.'views/structure/header.php'; ?>

  <!-- Add your site or application content here -->

  <div id="bg-login">
    <div class="container">
      <div class="row">
          <div class="col-lg-6 offset-lg-3 login-box">
            <form action="<?php echo CONTROLLER_PATH;?>loginController/logUser" id="loginUser" class="form-signin" method="POST">
                <div class="form-group">
                    <input id="nameUser" class="form-control" type="text" name="nameUser" placeholder="Identifiant" >
                </div>
                <div class="form-group">
                    <input id="pwdUser" class="form-control" type="password" name="pwdUser" value="" placeholder="Mot de passe" >
                    <input type="submit"  name="loginUser" class="btn btn-lg btn-block" value="Connexion"/>
                </div>
                <div class="form-group">
                        <?php if(isset($data)){ echo $data; } ?>
                    </div> 
                </div>
            </form>
          </div>
      </div>
   </div>

<?php require_once ROOT.'views/structure/footer.php'; ?>