<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ajaxModel
 *
 * @author emman
 */
class ajaxModel extends Model{
    //put your code here
    
    public function checkdate($date) {
        
        $bdd = parent::getBdd();
        $req = $bdd->prepare("SELECT * FROM intervention WHERE date_intervention = :date_inter");
        $req->bindValue(':date_inter', $date);
        $req->execute();
        if($req->rowCount() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    public function getTechData( ){
        
            $bdd = parent::getBdd();
            $req = $bdd->prepare("SELECT * FROM technicien");
            $req->execute();
            $tech_data = $req->fetchAll(PDO::FETCH_ASSOC);
            return $tech_data;
            
    }
    
    
    public function insertInterventions($interventions,$dayInter) {
        
         $bdd = parent::getBdd();
         
         //var_dump($interventions);
         
         foreach($interventions as $value) {
            
             $tech_id = $value['techid'];
             $type_inter = $value['name'];
             $nb_inter = $value['value'];
                   
             //insertion bdd 
             $req = $bdd->prepare("INSERT INTO `intervention`(date_intervention, type_intervention, nb_intervention, id_technicien) VALUES (:day_inter, :type_inter, :nb_inter, :tech_id)");
             $req->bindValue(':day_inter', $dayInter);
             $req->bindValue(':tech_id', $tech_id);
             $req->bindValue(':nb_inter', $nb_inter);
             $req->bindValue(':type_inter', $type_inter);
             $req->execute();
                   
         }
         
    }
    
    
      public function updateInterventions($interventions,$dayInter) {
        
         $bdd = parent::getBdd();
         
         
         // récupérer les id des interventions à update

         foreach($interventions as $inter){
             
             $tech_id = $inter['techid'];
             $type_inter = $inter['name'];
             $nb_inter = $inter['value'];
             
             //var_dump($dayInter); 
             
             $req = $bdd->prepare("SELECT * FROM intervention WHERE date_intervention = :day_inter AND id_technicien = :tech_id AND type_intervention = :type_inter");
             $req->bindValue(':day_inter', $dayInter);
             $req->bindValue(':tech_id', $tech_id);
             $req->bindValue(':type_inter', $type_inter);
             $req->execute();
             if($req->rowCount() > 0){
                 
                // pour cette date et ce tech, nous avons déjà des informations.
                $result = $req->fetchAll(PDO::FETCH_ASSOC);
 
                foreach($result as $key => $value){
                   
                    
                    var_dump($value);
                    $id_inter_to_update = $value['id'];
                    $nb_intervention = $value['nb_intervention'];
                    
                    // si nb inter vaut zero, alors on delete la ligne
                    if($nb_inter == 0){
                        
                        $delete = $bdd->prepare('DELETE FROM `intervention` WHERE id = :id');
                        $delete->bindParam(':id', $id_inter_to_update);
                        $delete->execute();
                    }else {

                        
                        $update = $bdd->prepare("UPDATE intervention SET nb_intervention = :nb_intervention WHERE id = :id");
                        $update->bindValue(':nb_intervention',$nb_inter);
                        $update->bindValue(':id',$id_inter_to_update);
                       
                        $resp = $update->execute();
                        
                      
                    }
                    
                }
                
            
                
             }else{
                 
                 // si val interventon différent de 0, on insère .
                 
                 if($nb_inter != 0){
                     
                    $req = $bdd->prepare("INSERT INTO `intervention`(date_intervention, type_intervention, nb_intervention, id_technicien) VALUES (:day_inter, :type_inter, :nb_inter, :tech_id)");
                    $req->bindValue(':day_inter', $dayInter);
                    $req->bindValue(':tech_id', $tech_id);
                    $req->bindValue(':nb_inter', $nb_inter);
                    $req->bindValue(':type_inter', $type_inter);
                    $req->execute();
                 
                 
             }
         }
   
    }
      }
    
    public function getDayData($day) {
        
        $bdd = parent::getBdd();
        $req = $bdd->prepare("SELECT * FROM intervention WHERE date_intervention = :date_inter");
        $req->bindValue(':date_inter', $day);
        $req->execute();
        $dayData = $req->fetchAll(PDO::FETCH_ASSOC);
        return $dayData;
    }
}
