<?php require_once ROOT.'views/structure/header.php'; ?>
<?php if (isset($_SESSION) && isset($_SESSION['role'])) { ?>
<div class="container">
      <div class="row">
          <div class="col-lg-12">
              <h2>Saisir les interventions du mois</h2>
              <div id="calendar"></div>
          </div>
      </div>
</div>
<?php }else{ exit; }?>
<?php require_once ROOT.'views/structure/footer.php'; ?>
