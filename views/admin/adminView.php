
<?php require_once ROOT.'views/structure/header.php'; ?>



<?php // var_dump($data); ?>

  <div class="container">
      <div class="row">
          <div class="col-lg-6 offset-lg-3">
            <form action="<?php echo CONTROLLER_PATH;?>adminController/displayTechCalendar" id="selectTech" class="form-signin" method="POST">
                <div class="form-group">
                    <h2>Séléctionnez le collaborateur</h2>
                    <select id="techSelect" name="technicien" class="form-control">   
                      <?php  foreach($data as $row) {
                          
                        echo "<option value='" . $row['id'] . "'>" . $row['nom'] ." ". $row['prenom']."</option>";
                        
                        }  ?>
                    </select>
                </div>
                <div class="form-group">
                    <input type="submit"  name="selectTech" class="btn btn-lg btn-success btn-block" value="Selectionner">
                </div>
            </form>
          </div>
      </div>
  </div>

<?php require_once ROOT.'views/structure/footer.php'; ?>