<?php if (isset($_SESSION) && isset($_SESSION['role'])) { ?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h2>Tous les prestataires</h2>
            <table class="table" >
                <thead>
                <th>id</th>
                <th>nom</th>
                <th>commentaire</th>
                </thead>
                <tbody id="listPresta">
                    
                </tbody>
            </table>
        </div>
        </div>
    <div class="row">
        <div class="col-lg-12">
            <h2>Ajouter un Prestataire</h2>
        </div>
                <div class="col-lg-12">
                     <label for="name_presta">Nom du prestataire</label>
                     <input type="text" name="name_presta" class="form-control" id="name_presta">
                </div>
                <div class="col-lg-12">
                    <label for="dayEnd">Informations complémentaires</label>
                    <textarea type="date" name="comment_presta" class="form-control" id="comment_presta"></textarea>
                </div>
                <div class="col-md-12">
                    <button type="button" style="margin-top:1rem;" class="btn btn-primary" id="addPresta">Ajouter le prestataire</button>
                </div>
                <div style="margin-top:1rem;" class="col-md-12" id="resp_presta">
            
                </div>
    </div>
     <div class="row">
        <div class="col-lg-12">
            <h2>Tous les techniciens</h2>
            <table class="table" >
                <thead>
                <th>id</th>
                <th>date de création</th>
                <th>nom</th>
                <th>prénom</th>
                </thead>
                <tbody id="listTech">
                    
                </tbody>
            </table>
        </div>
        </div>
    <div class="row">
        <div class="col-lg-12">
            <h2>Ajouter un technicien</h2>
        </div>
                <div class="col-lg-12">
                     <label for="name_presta">Nom du technicien</label>
                     <input type="text" name="name_presta" class="form-control" id="name_tech">
                </div>
                <div class="col-lg-12">
                     <label for="name_presta">Prénom du technicien</label>
                     <input type="text" name="name_presta" class="form-control" id="prename_tech">
                </div>
                <div class="col-md-12">
                    <button type="button" style="margin-top:1rem;" class="btn btn-primary" id="addTech">Ajouter le technicien</button>
                </div>
                <div style="margin-top:1rem;" class="col-md-12" id="resp_tech">
            
                </div>
    </div>
    <div class="row">
         <div class="col-lg-12">
       
        </div>
    </div>
 </div>
<?php }else{ exit; }?>