
<script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/js/bootstrap.min.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/js/moment.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/js/fullcalendar.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/locale/fr.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/js/config.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/js/app.js"></script>

</div><!--body content-->
<div class="modal fade" tabindex="-1" role="dialog" id="modalSaisie">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="popinTitle" data-date="" class="modal-title">Saisie des interventions <span  data-day="" id="dynamicDay"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <th></th>
                    <th>SAV<br/>intrusion</th>
                    <th>SAV<br/>Incendie</th>
                    <th>SAV<br/>Crt d'accès</th>
                    <th>SAV<br/>vidéo</th>
                    <th>SAV<br/>agression</th>
                    <th>SAV<br/>SAP</th>
                    <th>Maint<br/>Intrusion</th>
                    <th>Maint<br/>Incendie</th>
                    <th>Maint<br/>Crt accès</th>
                    <th>Maint<br/>vidéo</th>
                    <th>Maint<br/>agression</th>
                    <th>Maint<br/>SAP</th>
                    <th>Inst<br/>Intrusion</th>
                    <th>Inst<br/>incendie</th>
                    <th>Inst<br/>crt d'accès</th>
                    <th>Inst<br/>vidéo</th>
                    <th>Inst<br/>agression</th>
                    <th>Inst<br/>SAP</th>
                    </thead>
                    <tbody id="formInjector">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="saveNewDate">Sauvegarder</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
                
                <div id="modal-resp-saisie-success">
                    
                </div>
                <div id="modal-resp-saisie-success">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalEdit">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="popinTitle" data-date="" class="modal-title">Saisie des interventions <span  data-day="" id="dynamicDayEdit"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <th></th>
                    <th>SAV<br/>intrusion</th>
                    <th>SAV<br/>Incendie</th>
                    <th>SAV<br/>Crt d'accès</th>
                    <th>SAV<br/>vidéo</th>
                    <th>SAV<br/>agression</th>
                    <th>SAV<br/>SAP</th>
                    <th>Maint<br/>Intrusion</th>
                    <th>Maint<br/>Incendie</th>
                    <th>Maint<br/>Crt accès</th>
                    <th>Maint<br/>vidéo</th>
                    <th>Maint<br/>agression</th>
                    <th>Maint<br/>SAP</th>
                    <th>Inst<br/>Intrusion</th>
                    <th>Inst<br/>incendie</th>
                    <th>Inst<br/>crt d'accès</th>
                    <th>Inst<br/>vidéo</th>
                    <th>Inst<br/>agression</th>
                    <th>Inst<br/>SAP</th>
                    </thead>
                    <tbody id="formInjectorEdit">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="saveEditDate">Mettre à jour</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">fermer</button>
                    <div id="modal-resp-edit-success">
                        
                    </div>
                    <div id="modal-resp-edit-error">
                        
                    </div>
            </div>
        </div>
    </div>
</div>
<footer>
    <p class="mention">2018 - rapport.securicom-telesurveillance.fr</p>
</footer>
</body>

</html>
