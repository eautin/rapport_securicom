<script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
crossorigin="anonymous"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/js/bootstrap.min.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/js/moment.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/js/fullcalendar.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/calendar/locale/fr.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/js/config.js"></script>
<script src="<?php echo CONTROLLER_PATH; ?>views/assets/js/app_cumul.js"></script>

<div class="modal fade" tabindex="-1" role="dialog" id="modalCumul">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="popinTitle" data-date="" class="modal-title">Nombre d'interventions entre le <span  data-day="" id="popinDayStart"></span> et le <span id="popinDayEnd"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <th></th>
                    <th>SAV<br/>intrusion</th>
                    <th>SAV<br/>Incendie</th>
                    <th>SAV<br/>Crt d'accès</th>
                    <th>SAV<br/>vidéo</th>
                    <th>SAV<br/>agression</th>
                    <th>SAV<br/>SAP</th>
                    <th>Maint<br/>Intrusion</th>
                    <th>Maint<br/>Incendie</th>
                    <th>Maint<br/>Crt accès</th>
                    <th>Maint<br/>vidéo</th>
                    <th>Maint<br/>agression</th>
                    <th>Maint<br/>SAP</th>
                    <th>Inst<br/>Intrusion</th>
                    <th>Inst<br/>incendie</th>
                    <th>Inst<br/>crt d'accès</th>
                    <th>Inst<br/>vidéo</th>
                    <th>Inst<br/>agression</th>
                    <th>Inst<br/>SAP</th>
                    </thead>
                    <tbody id="formInjector">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
               
                <button type="button" class="btn btn-secondary" id="closePopin" data-dismiss="modal">fermer</button>
                
                <div id="modal-resp-saisie-success">
                    
                </div>
                <div id="modal-resp-saisie-success">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="modalInfo">
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
              
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Veuillez séléctionner une plage de jours pour obtenir le nombre d'intervention par technicien.</p>
            </div>
            <div class="modal-footer">
           
            </div>
        </div>
    </div>
</div>
<footer>
    <p class="mention">2018 - rapport.securicom-telesurveillance.fr</p>
</footer>
</body>

</html>